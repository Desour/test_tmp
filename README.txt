
If nothing else is written down, the code is distributed as Apache-2.0 (by DS).
This doesn't apply for example to code snippets where another author is given.
